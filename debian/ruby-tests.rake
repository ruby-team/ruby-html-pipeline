require 'rake/testtask'
Rake::TestTask.new(:test) do |test|
# github-linguist, gemoji not in archive
  test.test_files = FileList["test/**/*_test.rb"] - FileList["test/html/pipeline/emoji_filter_test.rb"]
  test.libs << '/usr/lib/ruby/vendor_ruby' << 'test'
  test.verbose = false
  test.warning = false
end


task :default => [:test]
